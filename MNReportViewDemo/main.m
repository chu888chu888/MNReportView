//
//  main.m
//  MNReportViewDemo
//
//  Created by Chenly on 13-12-27.
//  Copyright (c) 2013年 Chenly. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
